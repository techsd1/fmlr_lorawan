/*******************************************************************************
 * @file    stm32l1xx_hw.c
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   system hardware driver
 ******************************************************************************
 */
#include "hw.h"

/**
  * @brief Enters Low Power Stop Mode
  * @note ARM exists the function when waking up
  * @param none
  * @retval none
  */
void HW_EnterStopMode(void)
{
  BACKUP_PRIMASK();

  DISABLE_IRQ();

  HW_IoDeInit();

  /*clear wake up flag*/
  SET_BIT(PWR->CR, PWR_CR_CWUF);

  RESTORE_PRIMASK();

  /* Enter Stop Mode */
  HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
}

/**
  * @brief Enters Low Power Standby Mode
  * @param none
  * @retval none
  */
void HW_EnterStandbyMode(void)
{
  BACKUP_PRIMASK();

  DISABLE_IRQ();

  HW_IoDeInit();

  /*clear wake up flag*/
  SET_BIT(PWR->CR, PWR_CR_CWUF);

  RESTORE_PRIMASK();

  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);
  HW_RTC_StopAlarm();
  /* Enter Standby Mode */
  HAL_PWR_EnterSTANDBYMode();
}

/**
  * @brief Exists Low Power Stop Mode
  * @note Enable the pll at 32MHz
  * @param none
  * @retval none
  */
void HW_ExitStopMode(void)
{
  /* Disable IRQ while the MCU is not running on HSI */
  BACKUP_PRIMASK();

  DISABLE_IRQ();

  /* After wake-up from STOP reconfigure the system clock */
  /* Enable HSI */
  __HAL_RCC_HSI_ENABLE();

  /* Wait till HSI is ready */
  while (__HAL_RCC_GET_FLAG(RCC_FLAG_HSIRDY) == RESET) {}

  /* Enable PLL */
  __HAL_RCC_PLL_ENABLE();
  /* Wait till PLL is ready */
  while (__HAL_RCC_GET_FLAG(RCC_FLAG_PLLRDY) == RESET) {}

  /* Select PLL as system clock source */
  __HAL_RCC_SYSCLK_CONFIG(RCC_SYSCLKSOURCE_PLLCLK);

  /* Wait till PLL is used as system clock source */
  while (__HAL_RCC_GET_SYSCLK_SOURCE() != RCC_SYSCLKSOURCE_STATUS_PLLCLK) {}

  /* Initializes the peripherals*/
  HW_IoInit();

  RESTORE_PRIMASK();
}

/**
  * @brief Enters Low Power Sleep Mode
  * @note ARM exits the function when waking up
  * @param none
  * @retval none
  */
void HW_EnterSleepMode(void)
{
  HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

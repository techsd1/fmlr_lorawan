/******************************************************************************
 * @file    bsp.h
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   contains all hardware driver
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_H__
#define __BSP_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "sensors.h"

/** maximum number of sensors */
#define MAX_SENSORS 10


/** Device sensor configuration data */
typedef struct {
  FunctionalState reportBattLevel;  //< Flag to set if battery level is included in payload
} eSensorConfig_t;

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
 * @brief  initializes the sensor
 *
 * @param sensorConfig  sensor configuration data
 */
int8_t  BSP_sensor_Init(const volatile eSensorConfig_t* sensorConfig);

/**
 * @brief  sensor  read.
 *
 * @retval sensor_data
 */
const sensor_t* BSP_sensor_Read();

#ifdef __cplusplus
}
#endif

#endif /* __BSP_H__ */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

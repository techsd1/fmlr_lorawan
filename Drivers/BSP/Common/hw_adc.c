/*******************************************************************************
 * @file    stm32l1xx_hw.c
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   system hardware driver
 ******************************************************************************
 */
#include "hw.h"

/*!
 * \brief ADC Vbat measurement constants
 */

/* Internal voltage reference, parameter VREFINT_CAL*/
#define VREFINT_CAL       ((uint16_t*) ((uint32_t) 0x1FF80078))
#define TS_CAL1           ((uint16_t*) ((uint32_t) 0x1FF8007A))
#define TS_CAL2           ((uint16_t*) ((uint32_t) 0x1FF8007E))
#define LORAWAN_MAX_BAT   254

/** Definition of adc resolution and range */
#define ADC_RESOLUTION    ADC_RESOLUTION_12B
#define ADC_RANGE         4096

static ADC_HandleTypeDef hadc;
/*!
 * Flag to indicate if the ADC is Initialized
 */
static bool AdcInitialized = false;

uint16_t HW_GetTemperatureLevel(void)
{
  uint16_t battLevel = HW_AdcReadChannel(ADC_CHANNEL_VREFINT);
  float batteryLevelmV = (((float)VDDA_VREFINT_CAL * (*VREFINT_CAL)) / battLevel);

  uint16_t tempLevel = HW_AdcReadChannel(ADC_CHANNEL_TEMPSENSOR);
  float tempVal = (float)VDDA_VREFINT_CAL * tempLevel / batteryLevelmV;
  tempVal = 80.0f / ((*TS_CAL2) - (*TS_CAL1)) * (tempVal - (*TS_CAL1)) + 30.0f;
  return tempVal * 10.0f;
}

/**
  * @brief This function return the battery level
  * @param none
  * @retval the battery level  1 (very low) to 254 (fully charged)
  */
uint8_t HW_GetBatteryLevel(void)
{
  uint8_t batteryLevel = 0;
  uint16_t measuredLevel = 0;
  uint32_t batteryLevelmV;

  measuredLevel = HW_AdcReadChannel(ADC_CHANNEL_VREFINT);

  if (measuredLevel == 0) {
    batteryLevelmV = 0;
  } else {
    batteryLevelmV = (((uint32_t) VDDA_VREFINT_CAL * (*VREFINT_CAL)) / measuredLevel);
  }

  if (batteryLevelmV > VDD_BAT) {
    batteryLevel = LORAWAN_MAX_BAT;
  } else if (batteryLevelmV < VDD_MIN) {
    batteryLevel = 0;
  } else {
    batteryLevel = (((uint32_t)(batteryLevelmV - VDD_MIN) * LORAWAN_MAX_BAT) / (VDD_BAT - VDD_MIN));
  }
  return batteryLevel;
}

/**
  * @brief Read voltage on ADC pin
  *
  * Read ADC value from port and compensate with calibrated VREF
  *
  * @param ADC channel
  * @retval Voltage on port in [mV]
  */
float HW_GetVoltage(uint32_t channel)
{
  uint16_t battLevel = HW_AdcReadChannel(ADC_CHANNEL_VREFINT);
  float batteryLevelmV = (((float)VDDA_VREFINT_CAL * (*VREFINT_CAL)) / battLevel);

  uint16_t portLevel = HW_AdcReadChannel(channel);
  return (batteryLevelmV * portLevel) / ADC_RANGE;
}

/**
  * @brief This function initializes the ADC
  * @param none
  * @retval none
  */
void HW_AdcInit(void)
{
  if (AdcInitialized == false) {
    AdcInitialized = true;
    GPIO_InitTypeDef initStruct = {0};

    hadc.Instance  = ADC1;

    hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV4;
    hadc.Init.Resolution = ADC_RESOLUTION;
    hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
    /* hadc.Init.NbrOfConversion =1; not required since ADC_SCAN_DISABLE */
    hadc.Init.ContinuousConvMode = DISABLE;
    hadc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONVEDGE_NONE;

    hadc.Init.LowPowerAutoWait = ADC_AUTOWAIT_UNTIL_DATA_READ; /* ADC_DelaySelectionConfig( ADC1, ADC_DelayLength_Freeze ); */
    hadc.Init.LowPowerAutoPowerOff = ADC_AUTOPOWEROFF_IDLE_DELAY_PHASES;

    ADCCLK_ENABLE();

    HAL_ADC_Init(&hadc);

    initStruct.Mode = GPIO_MODE_ANALOG;
    initStruct.Pull = GPIO_NOPULL;
    initStruct.Speed = GPIO_SPEED_HIGH;

    HW_GPIO_Init(BAT_LEVEL_PORT, BAT_LEVEL_PIN, &initStruct);
  }
}

/**
  * @brief This function De-initializes the ADC
  * @param none
  * @retval none
  */
void HW_AdcDeInit(void)
{
  AdcInitialized = false;
}

/**
  * @brief This function De-initializes the ADC
  * @param Channel
  * @retval Value
  */
uint16_t HW_AdcReadChannel(uint32_t Channel)
{
  ADC_ChannelConfTypeDef adcConf;
  uint16_t adcData = 0;

  if (AdcInitialized == true) {
    /* wait the the Vrefint used by adc is set*/
    while (__HAL_PWR_GET_FLAG(PWR_FLAG_VREFINTRDY) == RESET) {};

    ADCCLK_ENABLE();

    /* configure adc channel */
    adcConf.Channel = Channel;
    adcConf.Rank = ADC_REGULAR_RANK_1;
    adcConf.SamplingTime = ADC_SAMPLETIME_192CYCLES;
    HAL_ADC_ConfigChannel(&hadc, &adcConf);

    /* Start the conversion process */
    HAL_ADC_Start(&hadc);

    /* Wait for the end of conversion */
    HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

    /* Get the converted value of regular channel */
    adcData = HAL_ADC_GetValue(&hadc);

    __HAL_ADC_DISABLE(&hadc);
    ADCCLK_DISABLE();
  }
  return adcData;
}

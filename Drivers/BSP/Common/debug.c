/******************************************************************************
  * @file    debug.c
  * @author  MCD Application Team
  * @version V1.0.2
  * @date    15-November-2016
  * @brief   debug API
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hw.h"

/**
  * @brief Initializes the debug
  * @param None
  * @retval None
  */
void DBG_Init(void)
{
#ifdef DEBUG
#if 0
  HAL_RCC_MCOConfig(RCC_MCO1, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
#endif

  __HAL_RCC_DBGMCU_CLK_ENABLE();

  HAL_DBGMCU_EnableDBGSleepMode();
  HAL_DBGMCU_EnableDBGStopMode();
  HAL_DBGMCU_EnableDBGStandbyMode();

#else /* DEBUG */
  /* sw interface off*/
  GPIO_InitTypeDef GPIO_InitStructure = {0};

  GPIO_InitStructure.Mode   = GPIO_MODE_ANALOG;
  GPIO_InitStructure.Pull   = GPIO_NOPULL;
  GPIO_InitStructure.Pin    = (GPIO_PIN_13 | GPIO_PIN_14);
  __GPIOA_CLK_ENABLE() ;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
  __GPIOA_CLK_DISABLE() ;

  __HAL_RCC_DBGMCU_CLK_ENABLE();
  HAL_DBGMCU_DisableDBGSleepMode();
  HAL_DBGMCU_DisableDBGStopMode();
  HAL_DBGMCU_DisableDBGStandbyMode();
  __HAL_RCC_DBGMCU_CLK_DISABLE();
#endif
}

/**
  * @brief Error_Handler
  * @param None
  * @retval None
  */
void Error_Handler(void)
{
  DBG_PRINTF("Error_Handler\n\r");
  while (1);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

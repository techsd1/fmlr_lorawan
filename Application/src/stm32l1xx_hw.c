/*******************************************************************************
 * @file    stm32l1xx_hw.c
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   system hardware driver
 ******************************************************************************
 */
#include "hw.h"
#include "radio.h"
#include "debug.h"
#include "bsp.h"

/*!
 * Flag to indicate if the MCU is Initialized
 */
static bool McuInitialized = false;

/**
  * @brief This function initializes the hardware
  * @param None
  * @retval None
  */
void HW_Init(const volatile eSensorConfig_t* sensorConfig)
{
  if (McuInitialized == false) {
    HW_AdcInit();
    Radio.IoInit();
    HW_SPI1_Init();
    HW_SPI2_Init();
    HW_RTC_Init();
    HW_UART_Init(HW_UART1_GetHandle(), 115200);
    HW_LED_Init(LED_GREEN);
    HW_LED_Init(LED_RED);

    McuInitialized = true;
  }
}

/**
  * @brief This function Deinitializes the hardware
  * @param None
  * @retval None
  */
void HW_DeInit(void)
{
  HW_SPI1_DeInit();
  HW_SPI2_DeInit();
  Radio.IoDeInit();
  HW_UART_DeInit(HW_UART1_GetHandle());
  HW_UART_DeInit(HW_UART2_GetHandle());
  HW_LED_DeInit(LED_GREEN);
  HW_LED_DeInit(LED_RED);

  McuInitialized = false;
}

/**
  * @brief This function Initializes the hardware Ios
  * @param None
  * @retval None
  */
void HW_IoInit(void)
{
  HW_SPI1_IoInit();
  HW_SPI2_IoInit();
  Radio.IoInit();
  HW_UART_IoInit(HW_UART1_GetHandle());
  // UART2 ??
  HW_LED_Init(LED_GREEN);
  HW_LED_Init(LED_RED);
}

/**
  * @brief This function Deinitializes the hardware Ios
  * @param None
  * @retval None
  */
void HW_IoDeInit(void)
{
  HW_SPI1_IoDeInit();
  HW_SPI2_IoDeInit();
  Radio.IoDeInit();
  HW_UART_IoDeInit(HW_UART1_GetHandle());
  // UART2 ??
  HW_LED_DeInit(LED_GREEN);
  HW_LED_DeInit(LED_RED);
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow :
  *            System Clock source            = PLL (HSI)
  *            SYSCLK(Hz)                     = 32000000
  *            HCLK(Hz)                       = 32000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 1
  *            APB2 Prescaler                 = 1
  *            HSI Frequency(Hz)              = 16000000
  *            PLLMUL                         = 6
  *            PLLDIV                         = 3
  *            Flash Latency(WS)              = 1
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* Enable HSE Oscillator and Activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState            = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL          = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PLLDIV          = RCC_PLL_DIV3;

  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    Error_Handler();
  }

  /* Set Voltage scale1 as MCU will run at 32MHz */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Poll VOSF bit of in PWR_CSR. Wait until it is reset to 0 */
  while (__HAL_PWR_GET_FLAG(PWR_FLAG_VOS) != RESET) {};

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
  clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK) {
    Error_Handler();
  }
}

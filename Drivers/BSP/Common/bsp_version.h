#ifndef __BSP_VERSION_H__
#define __BSP_VERSION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define BSP_VERSION_STRING   "1.0.0"

/**
 * @brief Print application version information
 *
 * @param name  Name of application
 * @param stack LoRaWAN stack version
 * @param app   Application version string
 */
void printSystemInformation(const char * name, uint32_t stack, const char * app);

#ifdef __cplusplus
}
#endif

#endif /*__BSP_VERSION_H__*/

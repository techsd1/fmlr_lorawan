/******************************************************************************
  * @file    lora.h
  * @author  MCD Application Team
  * @version V1.1.1
  * @date    01-June-2017
  * @brief   lora API to drive the lora state Machine
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __LORA_MAIN_H__
#define __LORA_MAIN_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "LoRaMac.h"
#include "region/Region.h"

/* Exported constants --------------------------------------------------------*/
#define LRWAN_MIRO_REVESION (uint32_t) 0x00000002  /* one (hex) digit for Miromico stack addon revision */
#define LRWAN_VERSION  (uint32_t) 0x00001120  /*3 next hex is i_cube release*/

/* Exported types ------------------------------------------------------------*/

/**
  * Lora Configuration
  */
typedef struct {
  FunctionalState otaa;        /*< ENABLE if over the air activation, DISABLE otherwise */
  FunctionalState duty_cycle;  /*< ENABLE if dutycyle is on, DISABLE otherwise */
  uint8_t DevEui[8];           /*< Device EUI */
  uint8_t AppEui[8];           /*< Application EUI */
  uint8_t AppKey[16];          /*< Application Key */
  uint8_t NwkSKey[16];         /*< Network Session Key */
  uint8_t AppSKey[16];         /*< Application Session Key */
  uint32_t DevAddr;            /*< Device address */
  uint32_t NetID;			   /*< Current network ID */
  uint8_t application_port;    /*< Application port we will receive to */
  FunctionalState ReqAck;      /*< ENABLE if acknowledge is requested */
} lora_configuration_t;

/*!
 * Application Data structure
 */
typedef struct {
  /*point to the LoRa App data buffer*/
  uint8_t* Buff;
  /*LoRa App data buffer size*/
  uint8_t BuffSize;
  /*Port on which the LoRa App is data is sent/ received*/
  uint8_t Port;
  /* Request confirmed messages */
  FunctionalState isTxConfirmed;
  /* Number of retrials in case of confirmed messages */
  uint8_t nbTrials;
} lora_AppData_t;

/*!
 * LoRa State Machine states
 */
typedef enum eDevicState {
  DEVICE_STATE_INIT,
  DEVICE_STATE_JOIN,
  DEVICE_STATE_JOINED,
  DEVICE_STATE_MANUAL,
  DEVICE_STATE_SEND,
  DEVICE_STATE_CYCLE,
  DEVICE_STATE_SLEEP
} DeviceState_t;

typedef enum eJoinedState {
  NETWORK_JOINED,
  NETWORK_NOT_JOINED,
} JoinedState_t;

#define LED_JOINED 500

/*!
 * LoRa State Machine states
 */
typedef enum eTxEventType {
  /*!
   * @brief AppdataTransmition issue based on timer every TxDutyCycleTime
   */
  TX_ON_TIMER,
  /*!
   * @brief AppdataTransmition external event plugged on OnSendEvent( )
   */
  TX_ON_EVENT,
  /*!
   * @brief AppdataTransmition issue based on timer every TxDutyCycleTime, sending is done manually
   */
  TX_ON_TIMER_MANUAL
} TxEventType_t;


/*!
 * LoRa State Machine states
 */
typedef struct sLoRaParam {
  /*!
   * @brief Event type
   *
   * @retval value  battery level ( 0: very low, 254: fully charged )
   */
  TxEventType_t TxEvent;
  /*!
   * @brief Application data transmission duty cycle in ms
   *
   * @note when TX_ON_TIMER Event type is selected
   */
  uint32_t TxDutyCycleTime;
  /*!
   * @brief LoRaWAN device class
   */
  DeviceClass_t Class;
  /*!
   * @brief Activation state of adaptativeDatarate
   */
  bool AdrEnable;
  /*!
   * @brief Uplink datarate, if AdrEnable is off
   */
  int8_t TxDatarate;
  /*!
   * @brief Enable or disable a public network
   *
   */
  bool EnablePublicNetwork;
  /*!
   * @brief Number of trials for the join request.
   */
  uint8_t NbTrials;

} LoRaParam_t;

/* Lora Main callbacks*/
typedef struct sLoRaMainCallback {
  /*!
   * @brief Get the current battery level
   *
   * @retval value  battery level ( 0: very low, 254: fully charged )
   */
  uint8_t (*BoardGetBatteryLevel)(void);

  /*!
   * @brief Gets the board 64 bits unique ID
   *
   * @param [IN] id Pointer to an array that will contain the Unique ID
   */
  void (*BoardGetUniqueId)(uint8_t* id);
  /*!
  * Returns a pseudo random seed generated using the MCU Unique ID
  *
  * @retval seed Generated pseudo random seed
  */
  uint32_t (*BoardGetRandomSeed)(void);
  /*!
   * @brief Prepares Tx Data to be sent on Lora network
   *
   * @param [IN] AppData struct to fill
   *
   * @param [IN] length is max length of buffer
   *
   * @param [IN] requests a confirmed Frame from the Network
   */
  void (*LoraTxData)(lora_AppData_t* AppData);
  /*!
   * @brief Process Rx Data received from Lora network
   *
   * @param [IN] AppData is a buffer to process
   *
   * @param [IN] port is a Application port on wicth Appdata will be sent
   *
   * @param [IN] length is the number of recieved bytes
   */
  void (*LoraRxData)(lora_AppData_t* AppData);
  /*!
   * @brief Call back for confirmed messages
   *
   * @param [IN] frmCnt uplink frame counter
   *
   * @param [IN] ack    true if ack on message received, false otherwise
   */
  void (*LoraConfirmation)(uint32_t frmCnt, bool ack);

} LoRaMainCallback_t;



/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
/**
 * @brief Lora Initialisation
 * @param [IN] LoRaMainCallback_t
 * @param [IN] application parmaters
 * @param [IN] hspi SPI device structure
 * @retval none
 */
void lora_Init(LoRaMainCallback_t* callbacks, const volatile LoRaParam_t* LoRaParam, SPI_HandleTypeDef* hspi);

/**
 * @brief run Lora classA state Machine
 * @param [IN] none
 * @retval none
 */
void lora_fsm(void);

/**
 * @brief functionl requesting loRa state machine to send data
 * @note function to link in mode TX_ON_EVENT
 * @param  none
 * @retval none
 */
void OnSendEvent(void);

/**
 * @brief API returns the state of the lora state machine
 * @note return @DeviceState_t state
 * @param [IN] none
 * @retval return @FlagStatus
  */
DeviceState_t lora_getDeviceState(void);
/**
 * @brief  Get whether or not acknowledgment is required
 * @param  None
 * @retval ENABLE or DISABLE
 */
FunctionalState lora_config_reqack_get(void);


/**
 * @brief  Get the application port we will receive the data to
 * @param  None
 * @retval The application port
 */
uint8_t lora_config_application_port_get(void);
#ifdef __cplusplus
}
#endif

#endif /*__LORA_MAIN_H__*/


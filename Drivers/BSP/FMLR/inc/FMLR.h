/**
  ******************************************************************************
  * @file    FMLR.h
  * @author  Alex Raimondi
  * @version V1.0.0
  * @date    17-Dec-2016
  * @brief   This file contains definitions for:
  *          - LEDs available on FMLR
  *          - Flash device on FMLR
  ******************************************************************************
  */

/** @addtogroup BSP
  * @{
  */

/** @addtogroup MIROMICO_FMLR
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FMLR_H
#define __FMLR_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"


/** @defgroup MIROMICO_FMLR_Exported_Types Exported Types
  * @{
  */
typedef enum {
  LED_RED,
  LED_GREEN
} Led_TypeDef;

// LED convenience macros
#define LED_Toggle( x )                 HW_LED_Toggle( x );
#define LED_On( x )                     HW_LED_On( x );
#define LED_Off( x )                    HW_LED_Off( x );

/**
  * @}
  */

/** @defgroup MIROMICO_FMLR_Exported_Constants Exported Constants
  * @{
  */

/**
  * @brief  Define for MIROMICO_FMLR board
  */
#define SPI1_MOSI_PORT                           GPIOA
#define SPI1_MOSI_PIN                            GPIO_PIN_7

#define SPI1_MISO_PORT                           GPIOA
#define SPI1_MISO_PIN                            GPIO_PIN_6

#define SPI1_SCLK_PORT                           GPIOA
#define SPI1_SCLK_PIN                            GPIO_PIN_5

#define SPI1_NSS_PORT                            GPIOA
#define SPI1_NSS_PIN                             GPIO_PIN_3

/* LORA I/O definition */
#define RADIO_RESET_PORT                          GPIOB
#define RADIO_RESET_PIN                           GPIO_PIN_3

// Radio MOSI
#define SPI2_MOSI_PORT                           GPIOB
#define SPI2_MOSI_PIN                            GPIO_PIN_15

// Radio MISO
#define SPI2_MISO_PORT                           GPIOB
#define SPI2_MISO_PIN                            GPIO_PIN_14

// Radio SCLK
#define SPI2_SCLK_PORT                           GPIOB
#define SPI2_SCLK_PIN                            GPIO_PIN_13

#define RADIO_NSS_PORT                            GPIOB
#define RADIO_NSS_PIN                             GPIO_PIN_12

#define RADIO_DIO_0_PORT                          GPIOB
#define RADIO_DIO_0_PIN                           GPIO_PIN_4

#define RADIO_DIO_1_PORT                          GPIOB
#define RADIO_DIO_1_PIN                           GPIO_PIN_7

#define RADIO_DIO_2_PORT                          GPIOB
#define RADIO_DIO_2_PIN                           GPIO_PIN_6

#define RADIO_DIO_3_PORT                          GPIOB
#define RADIO_DIO_3_PIN                           GPIO_PIN_5

#define RADIO_DIO_4_PORT                          GPIOA
#define RADIO_DIO_4_PIN                           GPIO_PIN_1

#define RADIO_DIO_5_PORT                          GPIOB
#define RADIO_DIO_5_PIN                           GPIO_PIN_11

#define RADIO_ANTSW_PWR_PORT                      GPIOA
#define RADIO_ANTSW_PWR_PIN                       GPIO_PIN_15

/*  SPI MACRO redefinition */

#define SPI1_CLK_ENABLE()                __HAL_RCC_SPI1_CLK_ENABLE()
#define SPI1_AF                          GPIO_AF5_SPI1
#define SPI2_CLK_ENABLE()                __HAL_RCC_SPI2_CLK_ENABLE()
#define SPI2_AF                          GPIO_AF5_SPI2

/* ADC MACRO redefinition */
#define BAT_LEVEL_PORT  GPIOA //CRF2
#define BAT_LEVEL_PIN  GPIO_PIN_4
#define ADC_READ_CHANNEL                 ADC_CHANNEL_4
#define ADCCLK_ENABLE()                 __HAL_RCC_ADC1_CLK_ENABLE() ;
#define ADCCLK_DISABLE()                 __HAL_RCC_ADC1_CLK_DISABLE() ;

/* --------------------------- RTC HW definition -------------------------------- */

#define RTC_OUTPUT       DBG_RTC_OUTPUT

/* --------------------------- USART HW definition -------------------------------*/

/* Definition for USART1 Pins */
#define USART1_RX_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE()
#define USART1_TX_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE()

#define USART1_TX_PIN                  GPIO_PIN_9
#define USART1_TX_GPIO_PORT            GPIOA
#define USART1_TX_AF                   GPIO_AF7_USART1
#define USART1_RX_PIN                  GPIO_PIN_10
#define USART1_RX_GPIO_PORT            GPIOA
#define USART1_RX_AF                   GPIO_AF7_USART1

/* Definition for USART2 Pins */
#define USART2_RX_GPIO_CLK_ENABLE()     __HAL_RCC_GPIOA_CLK_ENABLE()
#define USART2_TX_GPIO_CLK_ENABLE()     __HAL_RCC_GPIOA_CLK_ENABLE()
#define USART2_TX_PIN                  GPIO_PIN_3
#define USART2_TX_GPIO_PORT            GPIOA
#define USART2_TX_AF                   GPIO_AF7_USART2
#define USART2_RX_PIN                  GPIO_PIN_2
#define USART2_RX_GPIO_PORT            GPIOA
#define USART2_RX_AF                   GPIO_AF7_USART2

/** @defgroup MIROMICO_FMLR_LED LED Constants
  * @{
  */
#define LEDn                             2

#define LEDG_PIN                         GPIO_PIN_2
#define LEDG_GPIO_PORT                   GPIOB
#define LEDG_OFF                         GPIO_PIN_SET

#define LEDR_PIN                         GPIO_PIN_0
#define LEDR_GPIO_PORT                   GPIOB
#define LEDR_OFF                         GPIO_PIN_SET

/**
  * @}
  */

/* --------------------------- DEBUG redefinition -------------------------------*/

#define __HAL_RCC_DBGMCU_CLK_ENABLE()
#define __HAL_RCC_DBGMCU_CLK_DISABLE()

/** @addtogroup MIROMICO_FMLR_Exported_Functions
  * @{
  */
uint32_t BSP_GetVersion(void);


/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __FMLR_H */

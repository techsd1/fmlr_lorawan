/******************************************************************************
 * @file    hw_bsp.h
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   board specific hardware include
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_BSP_H__
#define __HW_BSP_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_ll_bus.h"
#include "stm32l1xx_ll_rcc.h"
#include "stm32l1xx_ll_usart.h"
//#include "stm32l0xx_ll_gpio.h"

#ifdef __cplusplus
}
#endif

#endif /* __HW_BSP_H__ */

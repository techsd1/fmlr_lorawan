/******************************************************************************
 * @file    stm32l1xx_hw_conf.h
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   contains hardaware configuration Macros and Constants for stm32l1
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_CONF_L1_H__
#define __HW_CONF_L1_H__

#ifdef __cplusplus
extern "C" {
#endif

/** Define battery limits for current hardware */
#define VDDA_VREFINT_CAL         	((uint32_t) 3000)
#define VDD_BAT                  	((uint32_t) 3000)
#define VDD_MIN                  	1800

/* --------------------------- AS3935 definition -------------------------------*/

#define LIGH_IRQ_PIN              GPIO_PIN_13
#define LIGH_IRQ_GPIO_PORT        GPIOC

/* --------------------------- US-100 definition -------------------------------*/
#define US100_PWR_PORT  					GPIOA
#define US100_PWR_PIN   					GPIO_PIN_1

/* --------------------------- VL53L0X definition ------------------------------*/
#define VL53L0X_XSHUT_PORT				GPIOA
#define VL53L0X_XSHUT_PIN					GPIO_PIN_6

#define VL53L0X_INT_PORT					GPIOA
#define VL53L0X_INT_PIN						GPIO_PIN_5

#ifdef __cplusplus
}
#endif

#endif /* __HW_CONF_L1_H__ */

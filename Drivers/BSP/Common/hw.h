/******************************************************************************
 * @file    hw.h
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   contains all hardware driver
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_H__
#define __HW_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include "hw_conf.h"
#include "hw_gpio.h"
#include "hw_spi.h"
#include "hw_rtc.h"
#include "hw_led.h"
#include "hw_power.h"
#include "hw_id.h"
#include "hw_adc.h"
#include "hw_msp.h"
#include "hw_bsp.h"
#include "debug.h"

typedef enum {
  HW_UNLOCKED = 0x00U,
  HW_LOCKED   = 0x01U
} HW_LockTypeDef;

#define HW_LOCK(__HANDLE__)               \
  do {                                    \
    if ((__HANDLE__)->Lock == HW_LOCKED)  \
    {                                     \
      return;                             \
    }                                     \
    else                                  \
    {                                     \
      (__HANDLE__)->Lock = HW_LOCKED;     \
    }                                     \
  } while (0)

#define HW_UNLOCK(__HANDLE__)             \
  do {                                    \
    (__HANDLE__)->Lock = HW_UNLOCKED;     \
  } while (0)

#ifdef __cplusplus
}
#endif

#endif /* __HW_H__ */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

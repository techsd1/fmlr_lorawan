/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: Helper functions implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "utilities.h"
#include "hw.h"

/*!
 * Redefinition of rand() and srand() standard C functions.
 * These functions are redefined in order to get the same behavior across
 * different compiler toolchains implementations.
 */
// Standard random functions redefinition start
#define RAND_LOCAL_MAX 2147483647L

static uint32_t next = 1;

int32_t rand1(void)
{
  return ((next = next * 1103515245L + 12345L) % RAND_LOCAL_MAX);
}

void srand1(uint32_t seed)
{
  next = seed;
}
// Standard random functions redefinition end

int32_t randr(int32_t min, int32_t max)
{
  return (int32_t)rand1() % (max - min + 1) + min;
}

void memcpy1(uint8_t* dst, const uint8_t* src, uint16_t size)
{
  while (size--) {
    *dst++ = *src++;
  }
}

void memcpyr(uint8_t* dst, const uint8_t* src, uint16_t size)
{
  dst = dst + (size - 1);
  while (size--) {
    *dst-- = *src++;
  }
}

void memset1(uint8_t* dst, uint8_t value, uint16_t size)
{
  while (size--) {
    *dst++ = value;
  }
}

int8_t Nibble2HexChar(uint8_t a)
{
  if (a < 10) {
    return '0' + a;
  } else if (a < 16) {
    return 'A' + (a - 10);
  } else {
    return '?';
  }
}

HAL_StatusTypeDef eep_write_byte(const uint8_t* dst, const uint8_t data)
{
  return eep_write_buffer(dst, &data, 1);
}

HAL_StatusTypeDef eep_write_buffer(const uint8_t* dst, const uint8_t* src, uint16_t size)
{
  uint32_t dst_addr = (uint32_t)dst;

  HAL_FLASHEx_DATAEEPROM_Unlock();
  for (uint8_t i = 0; i < size; i++) {
    HAL_StatusTypeDef status = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, dst_addr + i, *(src + i));
    if (status != HAL_OK) { return status; }
  }
  HAL_FLASHEx_DATAEEPROM_Lock();
  return HAL_OK;
}


/******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @version V1.0.2
  * @date    15-November-2016
  * @brief   this is the main!
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hw.h"
#include "low_power.h"
#include "lora.h"
#include "bsp.h"
#include "timeServer.h"
#include "version.h"
#include "tiny_sscanf.h"

/* call back when LoRa will transmit a frame*/
static void LoraTxData(lora_AppData_t* AppData);

/* call back when LoRa has received a frame*/
static void LoraRxData(lora_AppData_t* AppData);

/* load call backs*/
static LoRaMainCallback_t LoRaMainCallbacks = {
  HW_GetBatteryLevel,
  HW_GetUniqueId,
  HW_GetRandomSeed,
  LoraTxData,
  LoraRxData
};

/*!
 * Specifies the state of the application LED
 */
static uint8_t AppLedStateOn = RESET;

/* !
 * Initializes the LoRa Parameters
 */
static LoRaParam_t LoRaParamInit = {
  TX_ON_TIMER,
  18000,         // default duty cycle
  CLASS_A,       // device class
  DISABLE,       // ADR enable / disable
  DR_0,          // data rate
  true,					 // public network
  3              // Join retrails
};

/** Device configuration data */
static eSensorConfig_t sensorConfig = { ENABLE };

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  /* STM32 HAL library initialization*/
  HAL_Init();

  /* Configure the system clock*/
  SystemClock_Config();

  /* Configure the debug mode*/
  DBG_Init();

  /* Configure the hardware*/
  HW_Init(&sensorConfig);

  printSystemInformation("MultisensorHub", VERSION, APP_VERSION_STRING);

  /** Initialize sensors */
  BSP_sensor_Init(&sensorConfig);

  /* Configure the Lora Stack*/
  lora_Init(&LoRaMainCallbacks, &LoRaParamInit, HW_SPI2_GetHandle());

  PRINTF("Ready\n\n");

  /* main loop*/
  while (1) {
    /* run the LoRa class A state machine*/
    lora_fsm();

    DISABLE_IRQ();
    /* if an interrupt has occurred after DISABLE_IRQ, it is kept pending
     * and cortex will not enter low power anyway  */
    if (lora_getDeviceState() == DEVICE_STATE_SLEEP) {
#ifndef LOW_POWER_DISABLE
      LowPower_Handler();
#endif
    }
    ENABLE_IRQ();
  }
}

static void LoraTxData(lora_AppData_t* AppData)
{
  // Maximum buffer size is passed in
  uint8_t length = AppData->BuffSize;
  // just to be safe...
  AppData->BuffSize = 0;

  const sensor_t* sensors = BSP_sensor_Read();
  uint8_t i = 0;
  uint8_t sensor_nr;
  int8_t n = 0;

  for (sensor_nr = 0; sensor_nr < MAX_SENSORS; sensor_nr++) {
    switch (sensors[sensor_nr].type) {
    case eNoSensor:
      // break will only abort case, we need to end loop
      goto sensors_done;

    case eSHT21:
      n = LPP_Temperature(sensor_nr, sensors[sensor_nr].data.humidity.t, AppData->Buff + i, &length);
      if (n > 0) {
        i += n;
        n = LPP_Humidity(sensor_nr, sensors[sensor_nr].data.humidity.rh, AppData->Buff + i, &length);
      }
      break;

    case eSTS21:
      n = LPP_Temperature(sensor_nr, sensors[sensor_nr].data.temperature.t, AppData->Buff + i, &length);
      break;

    default:
      break;
    }
    if (n > 0) {
      i += n;
    }
  }

sensors_done: {
    if (sensorConfig.reportBattLevel == ENABLE) {
      int16_t batteryLevel = (HW_GetBatteryLevel() * 100 + 127) / 254;
      PRINTF("batteryLevel=%d %% => ", batteryLevel);
      n = LPP_Analog(sensor_nr + 1, batteryLevel * 100, AppData->Buff + i, &length);
      if (n > 0) {
        i += n;
      }
    }

    AppData->BuffSize = i;
    AppData->Port = lora_config_application_port_get();
    AppData->isTxConfirmed = lora_config_reqack_get();

  }
}

static void LoraRxData(lora_AppData_t* AppData)
{
  if (AppData->Port == lora_config_application_port_get()) {
    if (AppData->BuffSize == 1) {
      AppLedStateOn = AppData->Buff[0] & 0x03;
      if (AppLedStateOn & 0x01) {
        LED_On(LED_GREEN);
      } else {
        LED_Off(LED_GREEN);
      }
      if (AppLedStateOn & 0x02) {
        LED_On(LED_RED);
      } else {
        LED_Off(LED_RED);
      }
    }
  }
}

